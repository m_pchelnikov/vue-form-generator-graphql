## Bind vue-form-generator and GraphQl

Plugin realize query building for GraphQL based on schema, also it makes model from schema with groups.

### API
`method (arguments) {return}`
<br>
`$buildModel (schema: object, [model: object ]) {model: object}`
<br>
`$buildQuery (schema: object, model: object, options*: object) {query: function, vars: function}`

```
options: {
  type: 'query' or `mutation` (default),
  fields: `fields that backend will return, comma separated`
}
```


### How to use
1 Include plugin to your project: `import vfgg from 'vue-form-generator-graphql'`, `Vue.use(vfgg)`

2 First, build model based on schema:
```js
this.schema = JSON.parse(res.data.form_for_example);
this.model = this.$buildModel(this.schema);
```
It can takes array of fields or groups

3 Then collect form data and create query/mutation:
```js
const qb = this.$buildQuery(this.schema, this.model, {
  name: 'QueryName',
  fields: 'status, message',
});

this.$apollo.mutate({
  client: 'carrier',
  mutation: gql(qb.query()),
  variables: qb.vars(includeNulls: boolean),
})
  .then(res => { ...
```

query/mutation name will be prefixed with 'Fetch' (ex. 'FetchQueryName')
