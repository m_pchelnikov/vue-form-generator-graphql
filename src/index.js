class VueFormGeneratorGraphQL {
  /**
   * Build flat model based on schema with groups
   * @param schema
   * @param model - initial model
   * @returns {{}}
   */
  buildModel(schema, model = {}) {
    if (!schema) {
      throw new Error('[vue-form-generator-graphql] $buildModel: Schema is not presented')
    }
    else if (typeof schema !== 'object') {
      throw new TypeError(`[vue-form-generator-graphql] $buildModel: Type of schema expected "object" instead of ${typeof schema}`)
    }
    else if (!!schema.groups && schema.groups.length === 0 && !schema.fields) {
      throw new Error('[vue-form-generator-graphql] $buildModel: Schema is empty')
    }

    // If was given array-like object
    if (schema.groups && !schema.groups.length) {
      schema.groups = Object.keys(schema.groups).map(item => schema.groups[item])
    }

    // Factory ?
    if (schema.groups && schema.groups.length) {
      schema.groups.forEach(group => {
        model = Util.createModel(group, model)
      });
    }
    else if (schema.fields) {
      model = Util.createModel(schema, model)
    }

    return model
  }

  /**
   * Method query builds query/mutation string for usage in GraphQL
   * Method vars builds object with variables
   * @param schema
   * @param model
   * @param options.name - required - name of query/mutation in UpperCamelCase
   * @param options.type (mutation) - 'query' or 'mutation'
   * @param options.fields (status, message) - fields to fetch as response
   * @param options.label (id) - label of ID in model objects
   * @param options.customTypesMap - object of custom types
   * @returns {*}
   */
  buildQuery(schema, model, options) {
    let opts = {};

    if (!schema) {
      throw new Error('[vue-form-generator-graphql] $buildQuery: Schema must be defined')
    }

    if (!options.name) {
      throw new Error('[vue-form-generator-graphql] $buildQuery: Query/mutation name must be defined')
    }

    if (!options.type) {
      opts.type = 'mutation'
    }

    if (!options.fields) {
      opts.fields = 'status, message'
    }

    if (!options.label) {
      opts.label = 'id'
    }

    Object.assign(opts, options);

    const util = new Util(schema, model, opts);

    return {
      query() {
        return `${ opts.type } Fetch${ opts.name } (
          ${ util.defineVariables() }
        ) {
          ${ util.convertName2type() } (
            ${ util.bindVariables() }
          ) {
            ${ opts.fields }
          }
        }`
      },

      vars(args) {
        return util.makeVars(args)
      },

      getType() {
        return util.convertName2type()
      },
    }
  }
}

class Util {
  constructor(schema, model, opts) {
    this.schema = schema;
    this.model = model;
    this.opts = opts;

    this.defineVariableType = Util.defineVariableType.bind(this);
  }

  /**
   * Checks variable type considering custom types and returns this type
   * @param item
   * @returns {*}
   */
  static defineVariableType(item) {
    let preDefinedType = findValueType.call(this, item);

    function findValueType(item) {
      let result;

      for (let i in this.schema.groups) {
        let group = this.schema.groups[i];

        result = group.fields.find(field => {
          return field.model === item
        });

        if (!!result) {
          return result.valueType
        }
      }
    }

    if (preDefinedType) {
      return preDefinedType
    }
    else if (this.opts.customTypesMap && this.opts.customTypesMap[item]) {
      return this.opts.customTypesMap[item]
    }
    else if (typeof this.model[item] === 'string') {
      return 'String'
    }
    else if (
      typeof this.model[item] === 'number' ||
      this.model[item] === null ||
      (typeof this.model[item] === 'object' && typeof this.model[item][this.opts.label] === 'number')
    ) {
      return this.model[item] % 1 === 0 ? 'Int' : 'Float'
    }
    else if (typeof this.model[item] === 'boolean') {
      return 'Boolean'
    }
    else if (this.model[item] && this.model[item].length) { // Array
      switch (typeof this.model[item][0]) {
        case 'string':
          return `[String]`;
          break;
        case 'number':
          return `[Int]`;
          break;
        default:
          console.warn(`[vue-form-generator-graphql] Type of elements in array ${item} is not defined`)
      }
    }
    else {
      return item
    }
  }

  /**
   * Converts query Name to Type (UpperCamelCase to lower_case)
   * @returns {string}
   */
  convertName2type() {
    return this.opts.name
      .replace(/([A-Z])/g, '_$1')
      .replace('_', '')
      .toLowerCase()
  };

  /**
   * Creates string of variables with its types
   * @returns {string}
   */
  defineVariables() {
    return Object.keys(this.model).map(item => {
      return '$' + item + ': ' + this.defineVariableType(item)
    }).join(',\r\n')
  };

  /**
   * Bind variables with its types
   * @returns {string}
   */

  // $order: order_input
  // order_input: $order
  bindVariables() {
    return Object.keys(this.model).map(item => {
      if (this.opts.customTypesMap && this.opts.customTypesMap[item]) {
        return this.defineVariableType(item) + ': $' + item
      }
      return item + ': $' + item
    }).join(',\r\n')
  };

  /**
   * Cleans model
   * @returns {{}}
   */
  makeVars(includeNulls = true) {
    let result = {};

    Object.keys(this.model).map(item => {
      if (typeof this.model[item] === 'object' && this.model[item] !== null) { // Object
        if (Object.keys(this.model[item]).some(el => this.model[item][el]) || includeNulls) result[item] = this.model[item]
      }
      else if (this.model[item] === null && includeNulls) { // Null
        result[item] = null
      }
      else if (this.model[item] !== null) { // Has value
        result[item] = this.model[item]
      }
    });

    return result
  };

  static createModel(group, model) {
    if (!group || !group.fields) {
      throw new Error('[vue-form-generator-graphql] createModel: No fields list provided')
    }

    group.fields.forEach(field => {
      if (!!field.model && !!field.default) {
        model[field.model] = field.default
      }
      else if (!!field.model && !!field.selectOptions) {
        field.values.forEach(value => {
          if (value.id === field.selectOptions.default) {
            model[field.model] = {
              id: value.id,
              name: value.name
            }
          }
        })
      }
      else {
        model[field.model] = null
      }
    });

    return model
  }
}

let vfgg = new VueFormGeneratorGraphQL();

export default VueFormGeneratorGraphQL.install = function (Vue) {
  Vue.prototype.$buildModel = (schema) => vfgg.buildModel(schema);
  Vue.prototype.$buildQuery = (schema, model, options) => vfgg.buildQuery(schema, model, options);
};
